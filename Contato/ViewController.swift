//
//  ViewController.swift
//  Contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit
import Alamofire
import Kingfisher

import UIKit
import Alamofire
import Kingfisher

struct Contato: Decodable {
    let actor: String
    let name: String
    let image: String
}

class ViewController: UIViewController,UITableViewDataSource{

    var listaDeContatos: [Contato] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.dataSource = self


        getPersonagem()
        
    }
    func getPersonagem() {
        AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of: [Contato].self) { response in
            if let personagem = response.value {
                self.listaDeContatos = personagem

            }
            self.tableview.reloadData()
           
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return listaDeContatos.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celula = tableview.dequeueReusableCell(withIdentifier: "MinhaCelula",for: indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        
     
        celula.ator.text = contato.actor
        celula.nomePersonagem.text = contato.name
        celula.imgPersonagem.kf.setImage(with: URL(string: contato.image)) 
        return celula
    }
    

    @IBOutlet weak var tableview: UITableView!
}


    

